// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var port     = process.env.PORT || 8080; // set our port

var mongoose   = require('mongoose');
mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
var Hotspot     = require('./app/models/hotspot');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /hotspot
// ----------------------------------------------------
router.route('/hotspots')

	// create a hotspot (accessed at POST http://localhost:8080/hotspot)
	.post(function(req, res) {
		
		var hotspot = new Hotspot();		// create a new instance of the Hotspot model
		hotspot.name = req.body.name;  // set the hotspot name (comes from the request)
		res.json({ message: 'Hotspot wants to be created! '});

		hotspot.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'Hotspot created!' });
		});

		
	})

	// get all the hotspot (accessed at GET http://localhost:8080/api/hotspot)
	.get(function(req, res) {
		Hotspot.find(function(err, hotspot) {
			if (err)
				res.send(err);

			res.json(hotspot);
		});
	});

// on routes that end in /hotspot/:hotspot_id
// ----------------------------------------------------
router.route('/hotspots/:hotspot_id')

	// get the hotspot with that id
	.get(function(req, res) {
		Hotspot.findById(req.params.hotspot_id, function(err, hotspot) {
			if (err)
				res.send(err);
			res.json(hotspot);
		});
	})

	// update the hotspot with this id
	.put(function(req, res) {
		Hotspot.findById(req.params.hotspot_id, function(err, hotspot) {

			if (err)
				res.send(err);

			hotspot.name = req.body.name;
			hotspot.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Hotspot updated!' });
			});

		});
	})

	// delete the hotspot with this id
	.delete(function(req, res) {
		Hotspot.remove({
			_id: req.params.hotspot_id
		}, function(err, hotspot) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
