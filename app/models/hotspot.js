var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var hotspotSchema   = new Schema({
	// define the schema for our user model
    category: {
        adrenalin: Boolean,
        clubbing: Boolean,
        culture: Boolean,
        drinks: Boolean,
        extraordinary: Boolean,
        food: Boolean,
        fun: Boolean,
        music: Boolean,
        other: Boolean,
        party: Boolean,
        relax: Boolean,
        romantic: Boolean
    },
    city: String,
    name: String,
    othertext: String,
    street: String,
    user: String,
    zipcode: String
});

module.exports = mongoose.model('Hotspot', hotspotSchema);